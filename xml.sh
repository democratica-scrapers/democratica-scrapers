#!/bin/bash

pdftohtml -xml -nodrm -zoom 1.5 -enc UTF-8 -noframes $1 $1.xml
