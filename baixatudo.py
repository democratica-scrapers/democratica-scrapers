import urllib2

urls = [['http://www.camara.gov.br/internet/plenario/notas/extraord/EM0911110900.pdf',
  'http://www.camara.gov.br/internet/plenario/notas/extraord/EN0911111900.pdf',
  'http://www.camara.gov.br/internet/plenario/notas/conjuntas/CN0911111000.pdf '],
 ['http://www.camara.gov.br/internet/plenario/notas/extraord/EM0811110900.pdf',
  'http://www.camara.gov.br/internet/plenario/notas/extraord/EN0811112250.pdf',
  'http://www.camara.gov.br/internet/plenario/notas/extraord/EV0811111330.pdf',
  'http://www.camara.gov.br/internet/plenario/notas/extraord/EV0811111750.pdf'],
 ['http://www.camara.gov.br/internet/plenario/notas/ordinari/V0711111400.pdf'],
 [],
 [],
 ['http://www.camara.gov.br/internet/plenario/notas/ordinari/M0411110900.pdf'],
 ['http://www.camara.gov.br/internet/plenario/notas/ordinari/V0311111400.pdf'],
 [],
 ['http://www.camara.gov.br/internet/plenario/notas/ordinari/V0111111400.pdf',
  'http://www.camara.gov.br/internet/plenario/notas/extraord/EM0111110900.pdf'],
 ['http://www.camara.gov.br/internet/plenario/notas/ordinari/V3110111400.pdf',
  'http://www.camara.gov.br/internet/plenario/notas/solene/hm3110111000.pdf'],
 [],
 [],
 [],
 ['http://www.camara.gov.br/internet/plenario/notas/ordinari/V2710111400.pdf',
  'http://www.camara.gov.br/internet/plenario/notas/extraord/EM2710110900.pdf'],
 ['http://www.camara.gov.br/internet/plenario/notas/ordinari/V2610111400.pdf',
  'http://www.camara.gov.br/internet/plenario/notas/extraord/EN2610112000.pdf',
  'http://www.camara.gov.br/internet/plenario/notas/conjuntas/CN2610111200.pdf '],
 ['http://www.camara.gov.br/internet/plenario/notas/ordinari/V2510111400.pdf',
  'http://www.camara.gov.br/internet/plenario/notas/extraord/EN2510111810.pdf',
  'http://www.camara.gov.br/internet/plenario/notas/solene/hm2510111030.pdf'],
 ['http://www.camara.gov.br/internet/plenario/notas/ordinari/v2410111400.pdf',
  'http://www.camara.gov.br/internet/plenario/notas/solene/hm2410111030.pdf'],
 [],
 [],
 ['http://www.camara.gov.br/internet/plenario/notas/ordinari/V2110110900.pdf',
  'http://www.camara.gov.br/internet/plenario/notas/solene/hv2110111430.pdf',
  'http://www.camara.gov.br/internet/plenario/notas/solene/hv2110111640.pdf'],
 ['http://www.camara.gov.br/internet/plenario/notas/ordinari/V2010111400.pdf'],
 ['http://www.camara.gov.br/internet/plenario/notas/ordinari/V1910111400.pdf',
  'http://www.camara.gov.br/internet/plenario/notas/extraord/EN1910111930.pdf'],
 ['http://www.camara.gov.br/internet/plenario/notas/ordinari/V1810111400.pdf'],
 ['http://www.camara.gov.br/internet/plenario/notas/ordinari/V1710111400.pdf'],
 [],
 [],
 ['http://www.camara.gov.br/internet/plenario/notas/ordinari/M1410110900.pdf',
  'http://www.camara.gov.br/internet/plenario/notas/solene/hv1410111500.pdf'],
 ['http://www.camara.gov.br/internet/plenario/notas/ordinari/V1310111400.pdf'],
 []]


def download(url):
    base_dir = 'pdfs/'
    file_name = url.split('/')[-1]
    pdf = urllib2.urlopen(url)
    output = open(base_dir + file_name.strip(),'wb')
    output.write(pdf.read())
    output.close()

def downloadAll(urls):
    for d in urls:
        for link in d:
            print 'Downloading ' + link
            download(link)
