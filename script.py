#-*- coding: utf-8 -*-
#import scraperwiki
import os
import urllib
import tempfile
import os
from webstore.client import Database
from lxml import etree, cssselect
import datetime

# Blank Python
def css(selector, xml):
    return cssselect.CSSSelector(selector)(xml)

def pdftoxml(pdfdata):
    """converts pdf file to xml file"""
    pdffout = tempfile.NamedTemporaryFile(suffix='.pdf')
    pdffout.write(pdfdata)
    pdffout.flush()

    xmlin = tempfile.NamedTemporaryFile(mode='r', suffix='.xml')
    tmpxml = xmlin.name # "temph.xml"
    cmd = '/usr/bin/pdftohtml -xml -nodrm -zoom 1.5 -enc UTF-8 -noframes "%s" "%s"' % (pdffout.name, os.path.splitext(tmpxml)[0])
    cmd = cmd + " >/dev/null 2>&1" # can't turn off output, so throw away even stderr yeuch
    os.system(cmd)

    pdffout.close()
    #xmlfin = open(tmpxml)
    xmldata = xmlin.read()
    xmlin.close()
    return xmldata

def montaLinks(data = datetime.date.today(), no_dias = 30):      
    urls = []        
    for i in range(1,no_dias):
        data = data - datetime.timedelta(1)
        print 'Pegando links de ' + data.isoformat()
        url = 'http://www.camara.gov.br/internet/plenario/notas/notas.asp?dia=%i&mes=%i&ano=%i' % (data.day,data.month,data.year)
        urls.append(scrapeLista(url))
    return urls
    
def scrapeLista(url):
    lista = urllib.urlopen(url)
    lista = etree.parse(lista, etree.HTMLParser())
    links = css('li a', lista)
    pdfs = []
    for l in links:
        pdfs.append('http://www.camara.gov.br' + l.get('href'))
    return pdfs

    
def scrapePdf(url):
    pdfdata = urllib.urlopen(url).read()
    pdfxml = pdftoxml(pdfdata)
    xml = etree.fromstring(pdfxml)
    return xml

def processaCapa(xml):
    capa = css('page[number=1]', xml) #preciso processar as infos da capa
    xml.remove(capa[0])
    return xml, capa

def processaPresentes(xml):
    presentes = css('page[number=2]', xml) #preciso processar as infos dos presentes
    xml.remove(presentes[0])
    return xml, presentes

def processaInfosessao(xml):
    #<text top="76" left="213" width="299" height="15" font="7"><b>CÂMARA DOS DEPUTADOS - DETAQ </b></text>
    #<text top="75" left="213" width="294" height="15" font="5"><b>CÂMARA DOS DEPUTADOS - DETAQ</b></text>
    detaq = css('text[top=75][left=213]', xml)
    for d in detaq:
        d.getparent().remove(d) #remove detaq de todas as páginas

    #<text top="76" left="654" width="141" height="15" font="7"><b>REDAÇÃO FINAL </b></text>
    #<text top="75" left="642" width="148" height="15" font="5"><b>SEM SUPERVISÃO</b></text>
    supervisao = css('text[top=75]', xml)
    for s in supervisao:
        s.getparent().remove(s) #remove todas as marcas de supervisao

    #<text top="95" left="213" width="221" height="15" font="7"><b>Número Sessão: 303.1.54.O </b></text>
    #<text top="94" left="213" width="217" height="15" font="5"><b>Número Sessão: 305.1.54.O</b></text>
    numero_sessao = css('text[top=94][left=213]', xml)
    for n in numero_sessao:
        n.getparent().remove(n) #remove todas as marcas de numero da sessao

    #<text top="95" left="634" width="161" height="15" font="7"><b>Tipo: Ordinária - CD </b></text>
    #<text top="94" left="634" width="157" height="15" font="5"><b>Tipo: Ordinária - CD</b></text>
    tipo_sessao = css('text[top=94]', xml)
    for t in tipo_sessao:
        t.getparent().remove(t) #remove todas as marcas do tipo da sessao

    #<text top="114" left="213" width="133" height="15" font="7"><b>Data: 31/10/2011 </b></text>
    #<text top="113" left="213" width="129" height="15" font="5"><b>Data: 01/11/2011</b></text>
    data_sessao = css('text[top=113][left=213]', xml)
    for d in data_sessao:
        d.getparent().remove(d) #remove todas as marcas da data da sessao

    #<text top="114" left="620" width="175" height="15" font="7"><b>Montagem: 1966/4176 </b></text>
    #<text top="113" left="702" width="88" height="15" font="5"><b>Montagem:</b></text>
    montagem_sessao = css('text[top=113]', xml)
    for m in montagem_sessao:
        m.getparent().remove(m) #remove todas as marcas da data da sessao

    #<text top="1200" left="793" width="15" height="27" font="6">3</text>
    no_pagina = css('text[top=1200]', xml)
    for p in no_pagina:
        p.getparent().remove(p) #remove todas os numeros de pagina

    infosessao = {
    'info' : detaq[0].getchildren()[0].text,
    'supervisao' : supervisao[0].getchildren()[0].text.strip(),
    'numero' : numero_sessao[0].getchildren()[0].text[15:], #hack pro acento
    'tipo' : tipo_sessao[0].getchildren()[0].text.strip('Tipo: '),
    'data' : data_sessao[0].getchildren()[0].text.strip('Data: '),
    'montagem' : montagem_sessao[0].getchildren()[0].text.strip('Montagem: ')
    }

    return xml, infosessao

def processaTextosessao(xml):
    texto = css('text', xml)
    discursos = []
    discurso = {}

    for linha in texto:
        if int(linha.get('left')) > 181: #ignora tudo que não for texto de discurso
            pass
        elif len(linha.getchildren()) > 0 and linha.getchildren()[0].tag == 'b': #se for negrito, provavelmente nome de orador (precisa resolver excessoes)
            discursos.append(discurso)
            discurso = {}
            
            orador = linha.getchildren()[0]
            discurso['orador'] = orador.text
            linha.remove(orador)
            txt = ''
            for t in linha.itertext():
                txt += t
            discurso['texto'] = unicode(orador.tail) + unicode(txt)
        else:
            if linha.text:
                if discurso.has_key('texto'):
                    txt = ''
                    for t in linha.itertext():
                        txt += t
                    discurso['texto'] += txt
            
    #print discursos
    return xml, discursos

def processaTudo(infosessao, textosessao, capa = '', presentes = ''):
    if not textosessao[0]:
        textosessao.pop(0)
    resultado = textosessao    
    for n, discurso in enumerate(resultado):
        resultado[n].update(infosessao)
    return resultado

def salvaSessao(resultado, reset=0):
    database = Database('0.0.0.0', 'test', 'gatherdev', port=5000)
    table = database['tmpdiscursos']
    if 'tmpdiscursos' in database.tables() and reset == 1:
        table.delete()
    table.writerows(resultado)
    return 'ok'

def rockndroll():
    path = 'pdfs/'
    listing = os.listdir(path)
    bosta_count = 0
    ok_count = 0
    for infile in listing:
        print 'Processing ' + infile
        try:
            xml = scrapePdf(path+infile)
            xml, capa = processaCapa(xml)
            xml, presentes = processaPresentes(xml)
            xml, infosessao = processaInfosessao(xml)
            xml, textosessao = processaTextosessao(xml)
            resultado = processaTudo(infosessao, textosessao)
            salvaSessao(textosessao, reset=1)
            ok_count += 1
        except:
            print 'Bosta processing ' + infile
            bosta_count += 1
        print str(bosta_count) + ' bostas'
        print str(ok_count) + ' nao bostas'
